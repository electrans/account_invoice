from trytond.pool import PoolMeta


class Account(metaclass=PoolMeta):
    __name__ = 'bank.account'

    def get_rec_name(self, name):
        rec_name = super(Account, self).get_rec_name(name)
        rec_name = rec_name.replace('@', '-')
        if self.bank and self.bank.bic:
            rec_name += ' (' + self.bank.bic + ')'
        return rec_name
