# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelView, Workflow
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Or, If
from trytond.wizard import Wizard, Button, StateView, StateTransition
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
import shutil
import os
import re
from trytond.modules.electrans_tools.tools import generate_line_positions, move_lines


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    purchases_list = fields.Function(
        fields.Char('Purchases list',
            states={
                'readonly': Eval('state') != 'draft',
                'invisible': Eval('type') == 'out'}),
        'get_purchases_list',
        searcher='search_purchases_list')
    sale_reference = fields.Char(
        "Sale reference", states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('type') == 'in'},
        depends=['state', 'type'])
    sale = fields.Function(
        fields.Many2One('sale.sale',
            "Sale",
            states={'invisible': Eval('type') == 'in'},
            context={'hide_reference': True}),
        'get_sale',
        searcher='search_sale')
    order_notes = fields.Text(
        "Order Notes",
        states={'readonly': Eval('state') != 'draft'},
        depends=['state'],
        help="Complementari information to Order Number.\n"
        "It will be printer in each page under the Order Number.")
    operation_date = fields.Date(
        "Operation date",
        states={
            'readonly': Or(~Eval('state', '').in_(['draft']), Bool(Eval('number'))),
            'required': Eval('state', '').in_(['posted'])},
        depends=['state'])
    approved_by = fields.Function(
        fields.Many2One('res.user',
            "Approved by",
            states={'invisible': Eval('type') != 'in'}),
        'get_approved_by')
    approved_date = fields.Function(
        fields.Date(
            "Approved Date",
            states={'invisible': Eval('type') != 'in'}),
        'get_approved_date')
    due_date = fields.Function(
        fields.Date('Due Date'),
        'get_due_date')
    shipments = fields.Function(
        fields.One2Many('stock.shipment.out', None,
            "Shipments",
            states={'invisible': Eval('type') == 'in'}),
        'get_shipments')
    shipments_number = fields.Function(
        fields.Char("Shipments Number"),
        'get_shipments_number')
    quotation_numbers = fields.Function(
        fields.Char("Quotations Number"),
        'get_quotation_numbers')

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        if table.column_exist('customer_order_number'):
            table.column_rename('customer_order_number', 'order_reference')
        if table.column_exist('order_reference'):
            table.column_rename('order_reference', 'sale_reference')
        super(Invoice, cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._order.insert(0, ('invoice_date', 'DESC'))
        cls.reference.states['invisible'] = Eval('type') == 'out'
        cls.invoice_date.states['readonly'] = Or(~Eval('state', '').in_(['draft']), Bool(Eval('number')))

    @classmethod
    def copy(cls, invoices, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if not 'operation_date' in default:
            default['operation_date'] = None
        if not 'invoice_date' in default:
            default['invoice_date'] = None
        return super(Invoice, cls).copy(invoices, default=default)

    @classmethod
    def view_attributes(cls):
        return super(Invoice, cls).view_attributes() + [
            ('/form/notebook/page[@id="ediversa"]', 'states', {
                'invisible': Eval('type') == 'in'}),
            ('/form/notebook/page[@id="shipments"]', 'states', {
                'invisible': Eval('type') == 'in'})]

    @classmethod
    def search_sale(cls, name, clause):
        return [('lines.origin.sale',) + tuple(clause[1:3]) + ('sale.line',) + tuple(clause[3:])]

    def get_purchases_list(self, name=None):
        return ', '.join([p.number for p in self.purchases])

    @classmethod
    def search_purchases_list(cls, name, clause):
        return cls.search_purchases(name, clause)

    def get_sale(self, name=None):
        sale = None
        for line in self.lines:
            # If only the selection is filled, line.origin will be str instance
            if line.origin and not isinstance(line.origin, str):
                if line.origin.__name__ == 'sale.line' and line.origin.sale:
                    sale = line.origin.sale.id
                if (line.origin.__name__ == 'account.invoice.line' and
                        line.origin.invoice.sale):
                    sale = line.origin.invoice.sale.id

        return sale

    def _credit(self, **values):
        credit = super(Invoice, self)._credit(**values)
        for field in ['comment', 'order_notes', 'sale_reference']:
            setattr(credit, field, getattr(self, field))
        return credit

    def get_approved_by(self, name):
        att = self.get_main_attachment()
        return att[0].create_uid.id if att else None

    def get_approved_date(self, name):
        att = self.get_main_attachment()
        return att[0].create_date.date() if att else None

    def get_main_attachment(self):
        Attachment = Pool().get('ir.attachment')
        return Attachment.search([('main', '=', True), ('resource', '=', str(self))])

    def get_due_date(self, name):
        PaymentTerm = Pool().get('account.invoice.payment_term.line')
        if self.invoice_date and self.payment_term and self.payment_term.lines \
                and self.payment_term.lines[0].relativedeltas:
            due_date = self.invoice_date + self.payment_term.lines[0].relativedeltas[0].get()
            if self.party and self.payment_days:
                payment_days = [int(day) for day in self.payment_days.split()]
                with Transaction().set_context(account_payment_days=payment_days):
                    due_date = PaymentTerm(1).next_payment_day(due_date)
            return due_date

    @fields.depends('comment')
    def on_change_party(self):
        super(Invoice, self).on_change_party()
        if self.type == 'out' and not self.comment and self.party \
                and self.party.customer_invoice_comment:
            self.comment = self.party.customer_invoice_comment
        if self.send_address and not self.send_address.send_invoice:
            self.send_address = None

    @classmethod
    def set_invoice_dates(cls, invoices):
        pool = Pool()
        Date = pool.get('ir.date')
        Invoices = pool.get('account.invoice')
        to_save = []
        invoices_out = [i for i in invoices if i.type == 'out']
        for invoice in invoices_out:
            if not invoice.number:
                date = Date.today()
                if not invoice.invoice_date:
                    invoice.invoice_date = date
                    to_save.append(invoice)
                if not invoice.operation_date:
                    invoice.operation_date = date
                    if invoice not in to_save:
                        to_save.append(invoice)
        Invoices.save(to_save)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, invoices):
        Invoices = Pool().get('account.invoice')
        to_save = []
        super(Invoice, cls).draft(invoices)
        for invoice in invoices:
            if not invoice.number:
                invoice.invoice_date = None
                invoice.operation_date = None
                to_save.append(invoice)
        Invoices.save(to_save)

    @classmethod
    def check_line_origin(cls, invoices):
        Warning = Pool().get('res.user.warning')
        for invoice in invoices:
            to_warn = []
            for line in invoice.lines:
                if line.type == 'line' and not line.origin:
                    to_warn.append(line)
            if to_warn:
                key = 'invoice_%s' % invoice.id
                if Warning.check(key):
                    raise UserWarning(key, gettext(
                        'electrans_account_invoice.lines_without_origin',
                        position=", ".join(line.position for line in to_warn
                            if line.position),
                        product=", ".join(line.product.code for line in to_warn
                            if line.product and line.product.code)))

    @classmethod
    def validate_invoice(cls, invoices):
        Invoice.check_line_origin(invoices)
        Invoice.set_invoice_dates(invoices)
        super(Invoice, cls).validate_invoice(invoices)

    @classmethod
    def post(cls, invoices):
        Invoice.set_invoice_dates(invoices)
        Date = Pool().get('ir.date')
        for invoice in invoices:
            if not invoice.operation_date:
                invoice.operation_date = Date.today()
        super(Invoice, cls).post(invoices)

    @fields.depends('invoice_date', 'operation_date')
    def on_change_with_operation_date(self):
        if not self.operation_date:
            return self.invoice_date
        else:
            return self.operation_date

    def get_shipments(self, name=None):
        # Get the number of the shipments related
        # with the invoice lines origin if it is a sale.
        if self.type == 'out':
            shipments = []
            for line in self.lines:
                shipments.extend([move.shipment.id for move
                    in line.stock_moves if move.shipment])
            return shipments
        return []

    def get_shipments_number(self, name=None):
        # Get the number of the shipments related
        # with the invoice lines origin if it is a sale.
        shipments = set()
        for line in self.lines:
            for move in line.stock_moves:
                if move.shipment and move.shipment.number:
                    shipments.add(move.shipment.number)
        return ', '.join(shipment for shipment in shipments)

    def get_quotation_numbers(self, name=None):
        # Get the quotation number from the related sales
        quotations = set()
        for line in self.lines:
            if line.origin and line.origin.__name__ == 'sale.line':
                if (line.origin.sale.quotation
                        and line.origin.sale.quotation.number):
                    quotations.add(line.origin.sale.quotation.number)
        return ', '.join(quotation for quotation in quotations)


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    position = fields.Char('Position', states={
        'invisible': Eval('type').in_(['total', 'subtotal', 'subsubtotal'])
    }, depends=['type'])

    @classmethod
    def search_rec_name(cls, name, clause):
        res = super(InvoiceLine, cls).search_rec_name(name, clause)
        domain = ('OR', [('purchase',) + tuple(clause[1:])], [('description',) + tuple(clause[1:])])
        return ['OR', res, domain] if res else [domain]

    def _credit(self):
        line = super(InvoiceLine, self)._credit()
        setattr(line, 'position', self.position)
        return line

    @classmethod
    def copy(cls, lines, default=None):
        # Drag origin in lines
        new_lines = []
        if default is None:
            default = {}
        for line in lines:
            new_default = default.copy()
            new_default.setdefault('origin', line.origin)
            new_line, = super(InvoiceLine, cls).copy([line], default=new_default)
            new_lines.append(new_line)
        return new_lines


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    capture_invoice_source_path = fields.Char('Capture Invoice Source Path',
        help='Path of the server\'s directory from "Capture Invoice" functionality files will be pulled.')
    capture_invoice_destination_path = fields.Char('Capture Invoice Destination Path',
        help='Path of the server\'s directory from "Capture Invoice" functionality files will be generated.')
    pending_invoice_path = fields.Char('Pending Invoice Path')
    accounting_department_email = fields.Many2One(
        'party.contact_mechanism', 'Email from the Accounting Department')

    @classmethod
    def validate(cls, configurations):
        super(Configuration, cls).validate(configurations)
        for configuration in configurations:
            configuration.check_path()

    def check_path(self):
        for path in ['capture_invoice_source_path', 'capture_invoice_destination_path', 'pending_invoice_path']:
            if not getattr(self, path):
                return
            if not os.path.isdir(getattr(self, path)):
                raise UserError(
                    gettext('electrans_account_invoice.capture_invoice_path_not_exists', path=getattr(self, path)))

            if (not os.access(getattr(self, path), os.R_OK)
                    or not os.access(getattr(self, path), os.W_OK)):
                raise UserError(
                    gettext('electrans_account_invoice.capture_invoice_path_no_permission', path=getattr(self, path)))


class CaptureInvoiceStart(ModelView):
    'Capture Invoice Start'
    __name__ = 'capture.invoice.start'

    name = fields.Char('Name', readonly=True)
    attachment = fields.Binary('Attachment', filename='name', required=True)
    reference = fields.Char('Reference', readonly=True)


class CaptureInvoice(Wizard):
    'Capture Invoice'
    __name__ = 'capture.invoice'

    start = StateView('capture.invoice.start',
        'electrans_account_invoice.capture_invoice_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'move_attachment', 'tryton-ok', default=True)
        ])
    move_attachment = StateTransition()

    def default_start(self, fields):
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(Transaction().context.get('active_id'))
        defaults = {}
        if invoice.type != 'in':
            raise UserError(gettext('electrans_account_invoice.not_supplier_invoice'))
        defaults['reference'] = invoice.reference
        return defaults

    def transition_move_attachment(self):
        pool = Pool()
        Attachment = pool.get('ir.attachment')
        Invoice = pool.get('account.invoice')
        Configuration = pool.get('account.configuration')
        conf = Configuration(1)
        invoice = Invoice(Transaction().context.get('active_id'))
        if invoice.get_main_attachment():
            raise UserError(gettext('electrans_account_invoice.duplicated_main_attachment'))
        att_name = self.start.name
        source_path = conf.capture_invoice_source_path
        destination_path = conf.capture_invoice_destination_path
        pending_path = conf.pending_invoice_path
        source = source_path + "/" + att_name
        destination = destination_path + "/" + att_name
        pending = pending_path + "/" + att_name
        if os.path.isfile(source):
            if not os.path.isfile(destination):
                shutil.move(source, destination)
                Attachment.create([{'name': att_name, 'type': 'data', 'data': self.start.attachment,
                                    'resource': str(invoice), 'main': True}])
                if os.path.isfile(pending):
                    os.remove(pending)
                return 'end'
            else:
                raise UserError(
                    gettext('electrans_account_invoice.destination_file_already_exists', path=destination_path))

        else:
            raise UserError(gettext('electrans_account_invoice.no_source_file', path=source_path))


class InvoiceGenerateLinePositions(Wizard):
    """Invoice Generate Positions"""
    __name__ = 'account.invoice.generate_line_positions'

    start_state = 'generate_line_positions_start'
    generate_line_positions_start = StateTransition()

    def transition_generate_line_positions_start(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Line = pool.get('account.invoice.line')
        for invoice in Invoice.browse(Transaction().context['active_ids']):
            # Only save lines which position has changed
            modified_lines = generate_line_positions(invoice.lines)
            Line.save(modified_lines)
        return 'end'


class InvoiceTaxChange(Wizard):
    """Invoice Tax Change"""
    __name__ = 'account.invoice.invoice_tax_change'

    start = StateView('account.invoice.invoice_tax_change.start',
        'electrans_account_invoice.invoice_tax_change_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'tax_change_start', 'tryton-ok', default=True),
        ])
    tax_change_start = StateTransition()

    def default_start(self, fields):
        if self.record.state != 'draft':
            raise UserError(gettext('electrans_account_invoice.not_draft_in_change_tax'))
        return {
            'invoice': self.record.id,
            'company': self.record.company.id,
            'type': self.record.type,
            }

    def transition_tax_change_start(self):
        """
        This function changes the taxes of all the lines of the invoice at the same time
        """
        pool = Pool()
        Line = pool.get('account.invoice.line')
        for line in self.record.lines:
            line.taxes = (self.start.tax,)
        Line.save(self.record.lines)
        self.record.on_change_lines()
        self.record.save()
        return 'end'


class InvoiceTaxChangeStart(ModelView):
    'Invoice Tax Change Start'
    __name__ = 'account.invoice.invoice_tax_change.start'

    invoice = fields.Many2One('account.invoice', 'Invoice')
    company = fields.Many2One('company.company', 'Company')
    type = fields.Char('type')
    tax = fields.Many2One('account.tax', 'Tax',
        domain=[('parent', '=', None),
            ['OR',
                ('group', '=', None),
                ('group.kind', 'in', If(Eval('type') == 'out',
                    ['sale', 'both'],
                    ['purchase', 'both'])
            )],
            ('company', '=', Eval('company', -1)),
            ], depends=['invoice', 'type', 'company'])


class InvoiceLineMoveStart(ModelView):
    'Invoice Line Move Start'
    __name__ = 'invoice.line_move.start'

    lines_to_move = fields.One2Many(
        'account.invoice.line', None, "Lines to move",
        required=True,
        domain=[('invoice', '=', Eval('context', {}).get('active_id', -1))])
    position = fields.Selection([
        ('before', 'Before'),
        ('after', 'After')],
        "Position", required=True)
    line_position = fields.Many2One(
        'account.invoice.line', "Line Position",
        domain=[('invoice', '=', Eval('context', {}).get('active_id', -1))])


class InvoiceLineMove(Wizard):
    'Invoice Line Moves'
    __name__ = 'invoice.line_move'

    start = StateTransition()
    ask_invoice_line = StateView('invoice.line_move.start',
                      'electrans_account_invoice.invoice_line_move_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Move', 'move', 'tryton-ok', default=True),
                      ])
    move = StateTransition()

    def transition_start(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice(Transaction().context['active_id'])
        if invoice.state != 'draft':
            raise UserError(gettext('electrans_account_invoice.not_invoice_draft'))
        return 'ask_invoice_line'

    def transition_move(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice(Transaction().context['active_id'])
        move_lines(invoice.lines, self.ask_invoice_line.lines_to_move, self.ask_invoice_line.position,
                   self.ask_invoice_line.line_position)
        return 'end'


class RemoveLineDescriptionPosition(Wizard):
    'Remove Line Description Positions'
    __name__ = 'invoice.line_remove_description_position'

    start = StateTransition()

    def transition_start(self):
        InvoiceLine = Pool().get('account.invoice.line')
        to_save = []
        for invoice in self.records:
            if invoice.state != 'draft':
                raise UserError(
                    gettext('electrans_account_invoice.not_invoice_draft'))
            for line in invoice.lines:
                if line.description and line.description.startswith('Pos.:'):
                    # Removes string 'Pos.:X' from description where X is any
                    # string between 0 and infinite chars until a \n
                    line.description = re.sub(
                        r'Pos\.:.*', '', line.description).strip()
                    to_save.append(line)
                if line.description.startswith('Price list pos.:'):
                    # Removes string 'Price list pos.:X' from description where
                    # X is any string between 0 and infinite chars until a \n
                    line.description = re.sub(
                        r'Price list pos\.:.*', '', line.description).strip()
                    if line not in to_save:
                        to_save.append(line)

        InvoiceLine.save(to_save)
        return 'end'
