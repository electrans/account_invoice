# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import bank
from . import invoice
from . import party


def register():
    Pool.register(
        bank.Account,
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.InvoiceLineMoveStart,
        invoice.CaptureInvoiceStart,
        invoice.Configuration,
        invoice.InvoiceTaxChangeStart,
        party.Party,
        module='electrans_account_invoice', type_='model')

    Pool.register(
        invoice.CaptureInvoice,
        invoice.InvoiceGenerateLinePositions,
        invoice.InvoiceLineMove,
        invoice.InvoiceTaxChange,
        invoice.RemoveLineDescriptionPosition,
        module='electrans_account_invoice', type_='wizard')
