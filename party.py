# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    customer_invoice_comment = fields.Text(
        'Customer Invoice Comment',
        help="This comment will be used in the customer invoice.")

    def address_get(self, type=None):
        address = super(Party, self).address_get(type)
        if type == 'send_invoice' and address and not address.send_invoice:
            address = None
        return address
