# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import datetime
from decimal import Decimal
import trytond.tests.test_tryton
from trytond.modules.company.tests import create_company, set_company, create_employee
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.exceptions import UserError, UserWarning
from trytond.modules.electrans_tools.tests.test_tools import (
    create_fiscalyear_and_chart, get_accounts, create_product,
    create_account_category)
from trytond.transaction import Transaction


class TestAccountInvoiceCase(ModuleTestCase):
    'Test module'
    module = 'electrans_account_invoice'

    def create_payment_term(self):
        PaymentTerm = Pool().get('account.invoice.payment_term')
        term, = PaymentTerm.create([{
            'name': 'Payment term',
            'lines': [
                ('create', [{
                    'type': 'remainder',
                    'relativedeltas': [
                        ('create', [{
                            'sequence': 0,
                            'days': 0,
                            'months': 0,
                            'weeks': 0,
                        }])],
                }])],
        }])
        return term

    @with_transaction()
    def test_move_invoice_lines(self):
        "Test Move Invoice Lines"
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        Journal = Pool().get('account.journal')
        company = create_company()
        with set_company(company):
            employee = create_employee(company, 'Party1')
            self.assertEqual(len(employee.party.addresses), 1)
            employee.party.addresses[0].invoice = True
            employee.party.save()
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            party_account, = Account.search([('type.payable', '=', True),
                                             ('party_required', '=', True)], limit=1)
            employee.party.account_payable = party_account
            employee.party.save()
            journal, = Journal.search([('type', '=', 'expense')], limit=1)
            # Create invoice and lines
            invoice = Invoice()
            invoice.type = 'in'
            invoice.party = employee.party
            invoice.account = employee.party.account_payable
            invoice.invoice_address = employee.party.addresses[0]
            invoice.journal = journal
            invoice.save()
            # Create account for lines
            account_type, = AccountType.search([('expense', '=', True)], limit=1)
            account_line = Account(name='Line Account', type=account_type, closed=False)
            for i in range(0, 5):
                invoice_line = InvoiceLine()
                invoice_line.invoice = invoice
                invoice_line.quantity = 1
                invoice_line.account = account_line
                invoice_line.unit_price = Decimal('1.00')
                invoice_line.position = str(i+1) + '.'
                invoice_line.save()
            self.assertEqual(invoice.lines[0].position, '1.')
            self.assertEqual(invoice.lines[1].position, '2.')
            self.assertEqual(invoice.lines[2].position, '3.')
            self.assertEqual(invoice.lines[3].position, '4.')
            self.assertEqual(invoice.lines[4].position, '5.')
            # Execute Move Lines Wizard
            WizardMoveLines = pool.get(
                'invoice.line_move',
                type='wizard')
            # If invoice is in draft state and can move the lines after one defined line
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [invoice]
            with Transaction().set_context(active_id=invoice.id):
                wizard_move_lines.transition_start()
                wizard_move_lines.ask_invoice_line.lines_to_move = invoice.lines[0:4]
                wizard_move_lines.ask_invoice_line.position = 'after'
                wizard_move_lines.ask_invoice_line.line_position = invoice.lines[4]
                wizard_move_lines.transition_move()
            self.assertEqual(invoice.lines[0].position, '5.')
            self.assertEqual(invoice.lines[1].position, '1.')
            self.assertEqual(invoice.lines[2].position, '2.')
            self.assertEqual(invoice.lines[3].position, '3.')
            self.assertEqual(invoice.lines[4].position, '4.')
            # If invoice is in draft state and can move the lines before one defined line
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [invoice]
            with Transaction().set_context(active_id=invoice.id):
                wizard_move_lines.transition_start()
                wizard_move_lines.ask_invoice_line.lines_to_move =  invoice.lines[2:5]
                wizard_move_lines.ask_invoice_line.position = 'before'
                wizard_move_lines.ask_invoice_line.line_position = invoice.lines[1]
                wizard_move_lines.transition_move()
            self.assertEqual(invoice.lines[0].position, '5.')
            self.assertEqual(invoice.lines[1].position, '2.')
            self.assertEqual(invoice.lines[2].position, '3.')
            self.assertEqual(invoice.lines[3].position, '4.')
            self.assertEqual(invoice.lines[4].position, '1.')
            # If invoice is not in draft state
            invoice.invoice_date = datetime.date.today()
            invoice.state = 'validated'
            invoice.save()
            self.assertNotEqual(invoice.state, 'draft')
            session_id, _, _ = WizardMoveLines.create()
            wizard_move_lines = WizardMoveLines(session_id)
            wizard_move_lines.records = [invoice]
            with Transaction().set_context(active_id=invoice.id):
                with self.assertRaises(UserError):
                    wizard_move_lines.transition_start()

    @with_transaction()
    def test_credit_invoice_wizard(self):
        pool = Pool()
        Account = pool.get('account.account')
        Invoice = pool.get('account.invoice')
        Journal = pool.get('account.journal')
        Party = pool.get('party.party')
        ProductUom = pool.get('product.uom')
        Sale = pool.get('sale.sale')
        Tax = pool.get('account.tax')
        WizardCreditInvoice = pool.get('account.invoice.credit', type='wizard')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            revenue = accounts.get('revenue')
            expense = accounts.get('expense')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            receivable, = Account.search([
                ('type.receivable', '=', True),
            ])
            journal, = Journal.search([('type', '=', 'revenue')], limit=1)
            # Create Party
            party, = Party.create([{
                'name': 'Party',
                'addresses': [('create', [{}])],
            }])
            # Create payment term
            term = self.create_payment_term()
            template, product = create_product('Product 1', account_category)
            template.salable = True
            template.sale_uom = unit.id
            template.save()
            # Create sale
            sale, = Sale.create([{
                'party': party.id,
                'payment_term': term.id,
                'lines': [('create', [{
                    'product': product.id,
                    'quantity': Decimal('1'),
                    'unit_price': Decimal('10'),
                    'description': 'desc',
                    'unit': unit.id,
                }])],

            }])
            # Create invoice with
            invoice, = Invoice.create([{
                'type': 'out',
                'party': party.id,
                'invoice_address': party.addresses[0].id,
                'journal': journal.id,
                'account': receivable.id,
                'payment_term': term.id,
                'lines': [
                    ('create', [{
                        'invoice_type': 'out',
                        'type': 'line',
                        'sequence': 0,
                        'description': 'invoice_line',
                        'account': revenue.id,
                        'quantity': Decimal('1'),
                        'unit_price': Decimal('50.0'),
                        'origin': sale.lines[0],
                    }])],
            }])
            # Wizard test
            session_id, _, _ = WizardCreditInvoice.create()
            wizard_credit_invoice = WizardCreditInvoice(session_id)
            wizard_credit_invoice.records = ([invoice])
            with Transaction().set_context(active_ids=[invoice]):
                wizard_credit_invoice.start.with_refund = False
                wizard_credit_invoice.start.invoice_date = datetime.date.today()
                wizard_credit_invoice.model = Invoice
                # Returns a dict with the credit invoice values
                invoice_res_dict = wizard_credit_invoice._execute('credit')
                # Search of the Invoice with the ID of the invoice created by
                # executing 'credit' function
                invoice_res, = Invoice.search([(
                    'id', '=', invoice_res_dict['actions'][0][1]['res_id'][0])]
                )
                # Check if new invoice have correctly filled sale field
                self.assertEqual(invoice_res.sale.id, invoice.sale.id)

    @with_transaction()
    def test_validate_invoice_lines_without_origin(self):
        pool = Pool()
        Account = pool.get('account.account')
        Invoice = pool.get('account.invoice')
        Journal = pool.get('account.journal')
        Party = pool.get('party.party')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            revenue = accounts.get('revenue')
            expense = accounts.get('expense')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(tax, expense, revenue)
            receivable, = Account.search([
                ('type.receivable', '=', True),
            ])
            journal, = Journal.search([('type', '=', 'revenue')], limit=1)
            # Create Party
            party, = Party.create([{
                'name': 'Party',
                'addresses': [('create', [{}])],
            }])
            # Create payment term
            term = self.create_payment_term()
            # Create products
            template, product = create_product('Product 1', account_category)
            # Create invoice without origin defined in the line
            invoice_1, = Invoice.create([{
                        'invoice_date': datetime.date.today(),
                        'type': 'out',
                        'party': party.id,
                        'invoice_address': party.addresses[0].id,
                        'journal': journal.id,
                        'account': receivable.id,
                        'payment_term': term.id,
                        'lines': [
                            ('create', [{
                                'invoice_type': 'out',
                                'type': 'line',
                                'sequence': 0,
                                'description': 'invoice_line',
                                'account': revenue.id,
                                'quantity': 1,
                                'unit_price': Decimal('50.0'),
                                'product': product.id,
                                'unit': unit.id,
                                }])],
                        }])
            # Assert Invoice line doesn't have origin
            self.assertIs(invoice_1.lines[0].origin, None)
            # Assert Warning is being raised with a line without origin
            self.assertRaises(UserWarning, Invoice.validate_invoice, [invoice_1])


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestAccountInvoiceCase))
    return suite
